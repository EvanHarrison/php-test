<?php

/*
 * PDO Wrapper file.
 */

/*
 * A PDO Wrapper to simplify queries and responses.
 */

class PDO_wrapper {

    public $pdo;
    private $db_hostname;
    private $db_name;
    private $db_username;
    private $db_password;

    /*
     * Load the config and connect to the database.
     * 
     * @return void
     */

    public function __construct() {
        $this->get_db_details();
        $this->db_connect();
    }

    /*
     * Gets all config variables required from the config file
     * 
     * @return void
     */

    private function get_db_details() {
        require('./config.php');
        $this->db_hostname = $db_hostname;
        $this->db_name = $db_name;
        $this->db_username = $db_username;
        $this->db_password = $db_password;
    }

    /*
     * Connect to the database
     */

    private function db_connect() {
        $this->pdo = new PDO('mysql:host=' . $this->db_hostname . ';dbname=' . $this->db_name . ';charset=utf8mb4', $this->db_username, $this->db_password);
    }

    /*
     * Run a query on the database
     * 
     * @param string  $query    a unprepared query
     * @param boolean $is_array return an array for the response
     * @param array   $values   an array of values to be bound to the query
     * 
     * @return mixed
     */

    public function query($query, $is_array = false, $values = null) {
        $q = $this->pdo->prepare($query);
        $response = $q->execute($values);
        if ($is_array == true) {
            $result = $q->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } else {
            return $response;
        }
    }

}
