<?php

/*
 * Base core object
 */

/*
 * Core class
 */

class core {

    var $smarty;
    var $db;
    var $uri;
    var $action;
    var $user_session;
    var $is_protected_page = false;

    /*
     * Initializes a URI instance, PDO Wrapper, SMARTY.
     * Session is started by default.
     * 
     * @return void
     */

    public function __construct($action) {

        session_start();

        $this->action = $action;

        // Require PDO Wrapper
        require_once('./app/pdo_wrapper.php');
        $this->db = new PDO_wrapper();
        // Init URI
        require_once('./app/uri.php');
        $this->uri = new uri();
        // Init renderer
        //require_once('smarty_render.php');
        //$this->smarty = new render();
    }

    /*
     * By default we are setting the page to be protected and
     * checking for session permissions.
     * 
     * @return void
     */

    public function init($action) {
        $this->protect_page();
        $this->check_permissions();
    }

    /*
     * Sets the current class to be a protected page
     * 
     * @return void
     */

    private function protect_page() {
        $this->is_protected_page = true;
    }

    /*
     * Checks to see if the current instance is a protected
     * page & if we have a user session
     * 
     * @return redirect
     */

    private function check_permissions() {
        if ($this->is_protected_page && empty($this->user_session)) {
            header("Location: " . $this->uri->base_url . "login");
            exit;
        }
    }

}
