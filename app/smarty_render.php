<?php

/*
 * Smarty rendering wrapper
 */

/*
 * Rendering class
 */

class render {

    var $tpl;

    /*
     * Initialize smarty
     * @return void
     */

    public function __construct() {
        $this->init_smarty();
    }

    /*
     * Require the smarty class to $tpl
     * @return void
     */

    public function init_smarty() {
        require_once('/vendor/smarty/Smarty.class.php');
        $this->tpl = new Smarty;
        $this->tpl->debugging = false;
        $this->tpl->caching = false;
    }

}
