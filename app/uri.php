<?php

/*
 * URI related methods
 */

/*
 * URI request class
 */

class uri {

    var $base_url;
    var $uri;
    var $path;
    var $url;
    var $path_array;
    var $last_uri;

    /*
     * run the URI request method
     * @return void
     */

    public function __construct() {
        require('./config.php');
        $this->get_uri_request($base_url);
    }

    /*
     * Construct URI request data in the uri instance
     * @return void
     */

    public function get_uri_request($base_url) {
        // Get URI
        $this->uri = parse_url(filter_input(INPUT_SERVER, 'REQUEST_URI'));
        // Get the current requested path
        $this->path = str_replace($base_url, '', $this->uri['path']);
        // Break path into array
        $this->path_array = explode('/', $this->path);
        // Get the last part of the path
        $this->last_uri = array_pop((array_slice($this->path_array, -1)));
        // Get full URL
        $this->url = "http://" . filter_input(INPUT_SERVER, 'HTTP_HOST') . filter_input(INPUT_SERVER, 'REQUEST_URI');
    }

    /*
     * Get the current page URI
     * @return string
     */

    public function get_page() {
        return $this->path_array[0];
    }

}
