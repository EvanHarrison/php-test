<?php
/*
* Login Controller
*/

/*
* Login Controller Class
*/
class loginController extends core {

    var $action;
    var $names;
    
    /*
    * Calls the parent constructor, calls through action.
    * Runs the init method with the current action.
    * @return void
    */
    public function __construct($action) {
        parent::__construct($action);
        $this->init($this->action);
    }
    
    /*
    * Init takes the action and calls a related method
    * @return void
    */
    public function init($action) {
        switch ($action) {
            case 'post-login':
                $this->do_login();
                break;
            case 'login':
                // Login Page code here
                break;
            default:
                break;
        }
    }
    
    /*
    * Attemps to login to the user with user/password
    * @return void
    */
    private function do_login() {
        loginController::authenticate('evan','evan');
    }
    
    /*
    * Verifies a uusername and password from the database
    * @return boolean 
    */
    private function authenticate($username, $password) {

        $result = $this->db->query('SELECT password FROM users WHERE username = ?', true, array($username));
        $db_password = $result[0]['password'];

        if (password_verify($password, $db_password)) {
            return true;
        } else {
            return false;
        }
    }

}
