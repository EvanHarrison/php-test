<?php

/*
 * Home Controller
 */

/*
 * Home Controller Class
 */

class homeController extends core {

    var $action;
    var $names;

    /*
     * Calls the parent constructor, calls through action.
     * Runs the init method with the current action.
     * @return void
     */

    public function __construct($action) {
        parent::__construct($action);
        $this->init($this->action);
    }

    /*
     * Init takes the action and calls a related method
     * @return void
     */

    public function init($action) {
        //parent::init($action);
        switch ($action) {
            case 'browse':
                $this->browse_home();
                break;
            default:
                $this->browse_home();
                break;
        }
    }

    /*
     * Browse home method
     * @return void
     */

    public function browse_home() {
        $this->names = $this->get_names();
        print_r($this->names);
    }

    /*
     * Get all the names from a test table
     * @return array
     */

    public function get_names() {
        $names = $this->db->query('SELECT * from test_tbl', true);
        return $names;
    }

}
