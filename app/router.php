<?php

/*
 * Router
 */

/*
 * Router class gets the current URI and runs a method that matches in do_route()
 */

class router {

    var $page;
    var $uri;

    /*
     * Initializes a URI instance, runs do_route.
     * @return void
     */

    public function __construct() {
        // Init URI
        require_once('./app/uri.php');
        $this->uri = new uri();
        $this->do_route();
    }

    /*
     * Gets the current page from URI, runs whatever is needed for that route.
     * @return void
     */

    public function do_route() {
        switch ($this->uri->get_page()) {
            case 'home':
                $this->page_home();
                break;
            case 'login':
                $this->page_login();
                break;
            default:
                $this->page_home();
                break;
        }
    }

    /*
     * Runs an instance of the home page
     * @return void
     */

    public function page_home() {
        require_once('./app/core.php');
        require_once('./app/controllers/homeController.php');
        require_once('./app/views/homeView.php');
        $this->page = new homeView($this->uri->last_uri);
    }

    /*
     * Runs an instance of the login page.
     * @return void
     */

    public function page_login() {
        require_once('./app/core.php');
        require_once('./app/controllers/loginController.php');
        require_once('./app/views/loginView.php');
        $this->page = new loginView($this->uri->last_uri);
    }

}
